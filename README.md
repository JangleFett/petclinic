# AL Academy Petclinic

This version of Petclinic has been modified to work for the AL DevOps Academy and to use MySQL

## To compile the project

```
mvn -Dmaven.test.skip=true package
```

## Before compiling

You should make changes to the application.properties file located in **src/main/resources/application.properties**.  You only need to do this though if you want to change your default development properties.  Currently this is set up to work with localhost and petclinic/petclinic for the username/password.

The changes required are;

* Change the database connection
  ```
  spring.datasource.url=jdbc:mysql://localhost/petclinic
  ```
    * Change **localhost** to the IP or DNS name of your MySQL instance
    * Change **petclinic** to the name of your petclinic database
* Change the database username
  ```
  spring.datasource.username=petclinic
  ```
  * Change **petclinic** to your DBs username
* Change the database users password
  ```
  spring.datasource.password=petclinic
  ```
  * Change **petclinic** to your DB users password

You will need to make sure that the system you are going to compile this code on has;
* Java JDK 1.8 or above
* mvn
  - See https://www.javahelps.com/2017/10/install-apache-maven-on-linux.html

## Before running

You will need a MySQL database server and a username and password to enable the application to connect to.  The steps for this are (based on an Amazon Linux or RHEL server)

* Install mariadb-server and mariadb
* Execute the SQL scripts to build the Petclinic Schema in your database.  These scripts are located in **src/main/resources/db/mysql**
  * The order of these is as follows;
    * schema.sql
    * data.sql
* Create the **petclinic** user;
  ```
  CREATE USER 'petclinic'@'%' IDENTIFIED BY 'petclinic';
  GRANT ALL PRIVILEGES ON petclinic.* TO 'petclinic'@'%';
  ```

## To run

To enable PetClinic to run in different environments, e.g. DEV, QA, PROD, etc you should ensure that you have a different **application.properties** file for each of the environments.

When you run the PetClinic application you should be in the directory where the actual jar file is (e.g. if it's target then make sure you are in there).

You should ensure that the correct **application.properties** file is also in the same directory.  Ensure that you have set the **username**, **password** and the database server name in the properties file.  See the bit on **Before compiling** and the bit on changing the database.

Java applications look for an application.properties file in the same directory as the application **jar** file to override the default values.

```
java -jar target/*.jar
```
